#include "TcpServer.h"

#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <vector>

#pragma warning (disable: 4996)
// � ������ �������, � ������� Linker, � ������ Additional Dependancies ������� Ws2_32.lib
#pragma comment (lib,"ws2_32.lib")

struct TcpServer::PImpl
{
	std::atomic<bool> finish;

	WSADATA wsaData;
	SOCKET ListenSocket, ClientSocket;  // ���������� ����� � ����� ��� ��������
	sockaddr_in ServerAddr;  // ��� ����� ����� �������
	int err{};
	size_t maxlen = 512;  // ��� ������ � ������ �������
	char* recvbuf = new char[maxlen];  // ����� ������
	char* result_string = new char[maxlen];  // ����� ��������
};

//std::atomic<bool> TcpServer::PImpl::finish = false;

TcpServer::TcpServer()
	:d(new PImpl())
{}

/*static*/ int TcpServer::main(TcpServer& server)
{
	server.webcreate("127.0.0.1", 12345);
	return 0;
}

int TcpServer::close()
{
	d->finish = true;
	return 0;
}

int TcpServer::webcreate(const char* ipAddr, int ipPort)
{
	d->finish = false;

	// Initialize Winsock
	WSAStartup(MAKEWORD(2, 2), &d->wsaData);

	// Create a SOCKET for connecting to server
	d->ListenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	// Setup the TCP listening socket
	d->ServerAddr.sin_family = AF_INET;
	d->ServerAddr.sin_addr.s_addr = inet_addr(ipAddr);
	d->ServerAddr.sin_port = htons(ipPort);
	d->err = bind(d->ListenSocket, (sockaddr*)&d->ServerAddr, sizeof(d->ServerAddr));
	if (d->err == SOCKET_ERROR) {
		printf("bind failed: %d\n", WSAGetLastError());
		closesocket(d->ListenSocket);
		WSACleanup();
		return 1;
	}

	d->err = listen(d->ListenSocket, 50);
	if (d->err == SOCKET_ERROR) {
		printf("listen failed: %d\n", WSAGetLastError());
		closesocket(d->ListenSocket);
		WSACleanup();
		return 1;
	}
	while (!d->finish) {
		// Accept a client socket
		webrecive(d->recvbuf, d->maxlen);
		if (d->err > 0) {
			d->recvbuf[d->err] = 0;
			printf("Received query: %s\n", (char*)d->recvbuf);
			// ��������� ���������
			int result = 72;
			_snprintf_s(d->result_string, d->maxlen, d->maxlen, "OK %d\n", result);
			// ���������� ��������� �� ������
			websend(d->result_string, strlen(d->result_string));
			printf("Sent answer: %s\n", d->result_string);
		}
		else if (d->err == 0)
			printf("Connection closing...\n");
		else {
			printf("recv failed: %d\n", WSAGetLastError());
			closesocket(d->ClientSocket);
			WSACleanup();
			return 1;
		}

		// shutdown the connection since we're done
		closesocket(d->ClientSocket);
	}
	return 0;
}

int TcpServer::webrecive(char* buffer, size_t& len)
{
	d->ClientSocket = accept(d->ListenSocket, NULL, NULL);
	d->err = recv(d->ClientSocket, buffer, len, 0);
	return 0;
}

int TcpServer::websend(char* buffer, size_t len)
{
	send(d->ClientSocket, buffer, len, 0);
	return 0;
}

