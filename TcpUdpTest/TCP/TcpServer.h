#pragma once
#include <atomic>
#include <memory>

#include "../IWebServer.h"

class TcpServer: public IWebServer
{
	struct PImpl;
	std::shared_ptr<PImpl> d;
public:
	TcpServer();
	static int main(TcpServer& servId);
	int close();

	// IWebServer
	virtual int webcreate(const char* ipAddr, int port) override;
	virtual int webrecive(char* buffer, size_t& len) override;
	virtual int websend(char* buffer, size_t len) override;
};

