#include "TcpClient.h"

#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

#pragma warning (disable: 4996)
// � ������ �������, � ������� Linker, � ������ Additional Dependancies ������� Ws2_32.lib
#pragma comment (lib,"ws2_32.lib")

namespace
{
	int __cdecl mainTC(void)
	{
		WSADATA wsaData;
		SOCKET ConnectSocket;  // ���������� ����� � ����� ��� ��������
		sockaddr_in ServerAddr;  // ��� ����� ����� �������
		int err, maxlen = 512;  // ��� ������ � ������ �������
		char* recvbuf = new char[maxlen];  // ����� ������
		char* query = new char[maxlen];  // ����� ��������


		// Initialize Winsock
		WSAStartup(MAKEWORD(2, 2), &wsaData);

		// Connect to server
		ConnectSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

		ServerAddr.sin_family = AF_INET;
		ServerAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
		ServerAddr.sin_port = htons(12345);

		err = connect(ConnectSocket, (sockaddr*)&ServerAddr, sizeof(ServerAddr));

		if (err == SOCKET_ERROR) {
			printf("connect failed: %d\n", WSAGetLastError());
			closesocket(ConnectSocket);
			WSACleanup();
			return 1;
		}

		_snprintf_s(query, maxlen, maxlen, "CALC * 12 6\n");
		// ���������� ������ �� ������
		send(ConnectSocket, query, strlen(query), 0);
		printf("Sent: %s\n", query);

		// �������� ���������
		err = recv(ConnectSocket, recvbuf, maxlen, 0);
		if (err > 0) {
			recvbuf[err] = 0;
			printf("Result: %s\n", (char*)recvbuf);
		}
		else if (err == 0)
			printf("Connection closing...\n");
		else {
			printf("recv failed: %d\n", WSAGetLastError());
			closesocket(ConnectSocket);
			WSACleanup();
			return 1;
		}
		//// send 0 byte to finish 
		//send(ConnectSocket, 0, 0, 0);
		//// �������� ���������
		//err = recv(ConnectSocket, recvbuf, maxlen, 0);

		// shutdown the connection since we're done
		closesocket(ConnectSocket);

	}
}

int TcpClient::main()
{
	return mainTC();
}
