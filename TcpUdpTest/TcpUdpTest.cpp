﻿// TcpUdmTest.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <thread>

#include "TCP/TcpClient.h"
#include "TCP/TcpServer.h"
#include "UDP/UdpClient.h"
#include "UDP/UdpServer.h"

using namespace std;

int main(int argc, char** argv)
{
    cout << "Hello World!\n";
    if (argc < 2)
    {
        cout << endl << "=== TCP Server test ===" << endl;
        int servId{ 0 };
        TcpServer servTcp;
        auto tcpServ = thread([&](){TcpServer::main(servTcp); });
        TcpClient::main();

        servTcp.close();
        //TcpClient::main(); // to finish need read
        tcpServ.join();

        cout << endl << "=== UDP Server test ===" << endl;
        auto udpServ = thread(UdpServer::main);
        UdpClient::main();

        UdpServer::finish = true;
        //UdpClient::main(); // to finish need read
        udpServ.join();
    }
    cout << "finish\n";
}

