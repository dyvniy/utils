#pragma once
class IWebServer
{
public:
	// return error code
	virtual int webcreate(const char* ipAddr, int port) = 0;
	virtual int webrecive(char* buffer, size_t& len) = 0;
	virtual int websend(char* buffer, size_t len) = 0;
};

