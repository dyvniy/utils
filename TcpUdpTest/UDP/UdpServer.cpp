#include "UdpServer.h"

#pragma warning (disable: 4996)
// � ������ �������, � ������� Linker, � ������ Additional Dependancies ������� Ws2_32.lib
#pragma comment (lib,"ws2_32.lib")

std::atomic<bool> UdpServer::finish = false;

int UdpServer::main()
{
	UdpServer server;
	return server.execute();
}

int UdpServer::execute()
{
	finish = false;

	// Initialize Winsock
	WSAStartup(MAKEWORD(2, 2), &wsaData);

	// Create a SOCKET for connecting to server
	SendRecvSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	// Setup the TCP listening socket
	ServerAddr.sin_family = AF_INET;
	ServerAddr.sin_addr.s_addr = inet_addr("127.0.0.1"); // inet_pton(0, "127.0.0.1", 0); //
	ServerAddr.sin_port = htons(12345);
	err = bind(SendRecvSocket, (sockaddr*)&ServerAddr, sizeof(ServerAddr));
	if (err == SOCKET_ERROR) {
		printf("bind failed: %d\n", WSAGetLastError());
		closesocket(SendRecvSocket);
		WSACleanup();
		return 1;
	}

	while (!finish) {
		// Accept a client socket
		err = recvfrom(SendRecvSocket, recvbuf, maxlen, 0, (sockaddr*)&ClientAddr, &ClientAddrSize);
		if (err > 0) {
			recvbuf[err] = 0;
			printf("Received query: %s\n", (char*)recvbuf);
			// ��������� ���������
			int result = 72;
			_snprintf_s(result_string, maxlen, maxlen, "OK %d\n", result);
			// ���������� ��������� �� ������
			sendto(SendRecvSocket, result_string, strlen(result_string), 0, (sockaddr*)&ClientAddr, sizeof(ClientAddr));
			printf("Sent answer: %s\n", result_string);
		}
		else {
			printf("recv failed: %d\n", WSAGetLastError());
			closesocket(SendRecvSocket);
			WSACleanup();
			return 1;
		}
	}
}

