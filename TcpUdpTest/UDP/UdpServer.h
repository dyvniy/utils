#pragma once
#include <atomic>

#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

#include "../IWebServer.h"

class UdpServer //: public IWebServer
{
	WSADATA wsaData;
	SOCKET SendRecvSocket;  // ����� ��� ������ � ��������
	sockaddr_in ServerAddr, ClientAddr;  // ��� ����� ����� ������� � ��������
	const int maxlen = 512;
	int err, ClientAddrSize = sizeof(ClientAddr);  // ��� ������, ������ ������� � ������ ��������� ������
	char* recvbuf = new char[maxlen];  // ����� ������
	char* result_string = new char[maxlen];  // ����� ��������

	int execute();
public:
	static int main();
	static std::atomic<bool> finish;

	// IWebServer
	//virtual int webcreate(const char* ipAddr, int port);
	//virtual int webrecive(char* buffer, int& len);
	//virtual int websend(char* buffer, int& len);
};

